<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/9/26
 * Time: 0:45
 */

namespace tests;

use think\Container;

class TestCase extends \tianshupei\think\testing\TestCase
{
    protected $baseUrl = 'http://localhost';

    public function __construct(?string $name = null, array $data = [], string $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        require_once __DIR__ . '/../thinkphp/base.php';

        Container::get('app')->path(__DIR__ . '/../application/')->initialize();
    }
}